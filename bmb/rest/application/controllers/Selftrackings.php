<?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 header('Access-Control-Allow-Origin: *');

 class Selftrackings extends PIXOLO_Controller {
 	 function __construct()
 	 {
 	 	 parent::__construct();

        
        //ABOUT THIS TABLE
       $this->load->model('Selftracking_model','model');
        $this->tablename = "selftrackings";
       

 	 }

   	 public function index()
   	 {
   	 	  $message['json']=$this->model->get_all();
   	 	  $this->load->view('json', $message);
   	 }
    

 }