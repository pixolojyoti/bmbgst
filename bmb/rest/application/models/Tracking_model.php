<?php
 defined('BASEPATH') OR exit('No direct script access allowed');

 class Tracking_model extends PIXOLO_Model
 {
   public $_table = 'airwaybills';

 	 //Write functions here

     
     
     public function gettrackingdata($airway_number){
         $airway_number = substr($airway_number,1, strlen($airway_number)-2);

         $query=$this->db->query("SELECT * FROM `airwaybills` WHERE `airway_number` IN ($airway_number)")->result();
         

         foreach($query as $row){
             if($row->forwarding_company_id==1){
                 //GET SELF DATA
                 $row->type = "data";
                 $row->data = $this->db->query("SELECT * FROM `selftrackings` WHERE `airwaybill_id`=$row->id")->result();
             }else{
                 //GET URLS
                 $row->type = "url";
                 $forwarding_company = $this->db->query("SELECT `name`, `url` FROM `courierservices` WHERE `id` = $row->forwarding_company_id")->row();
                 $row->url = $forwarding_company->url;
                 $row->url = str_replace("trackid",$row->forwarding_number,$row->url);
                 $row->forwarding_company_name = $forwarding_company->name;
             };
         }
         return $query;

 }
 }

?>
