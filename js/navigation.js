var adminurl = "http://flybirdinternational.com/billmate/bmb/rest/index.php/";
//var adminurl = "http://localhost/bmbgst/bmb/rest/index.php/";
var navigationservice = angular.module('navigationservice', [])

    .factory('NavigationService', function ($http) {
        var navigation = navs;
        var version=new Date().getTime();
        return {
            getnav: function () {
                return navigation;
            },
            makeactive: function (menuname) {
                for (var i = 0; i < navigation.length; i++) {
                    if (navigation[i].appname == menuname) {
                        navigation[i].classis = "active";
                    } else {
                        navigation[i].classis = "";
                    }
                }
                return menuname;
            },
            dologin: function (userdata) {
                var formdata = new FormData();
                formdata.append('email', userdata.email);
                formdata.append('password', userdata.password);
                return $http({
                    url: adminurl + 'users/login',
                    method: 'POST',
                    headers: {
                        'Content-Type': undefined
                    },
                    data: formdata,
                    transformRequest: angular.identity

                });



            },

            /* CUSTOMERS PAGE */
            getallcustomers: function () {
                return $http.get(adminurl + "customer/getall", {
                    params: {
                        v:version+''
                    }
                });
            },
            getreports: function (fromdate, todate, customer, customer_contact, status) {
                return $http.get(adminurl + "invoice/getreports", {
                    params: {
                        'fromdate': fromdate,
                        'todate': todate,
                        'customer': customer,
                        'customer_contact': customer_contact,
                        'status': status,
                        'v': version+''
                    }
                });
            },
            updatetds: function (id, tds) {
                return $http.get(adminurl + "invoice/updatetds", {
                    params: {
                        'id': id,
                        'tds': tds,
                        'v': version+''
                    }
                });
            },
            getdatewisestats: function (fromdate, todate, customer, status) {
                return $http.get(adminurl + "invoice/getdatewisestats", {
                    params: {
                        'fromdate': fromdate,
                        'todate': todate,
                        'customer': customer,
                        'status': status,
                        'v': version+''
                    }
                });
            },
            //            getallcustomers: function () {
            //                return $http.get(adminurl + "customer/getall", {
            //                    params: {}
            //                });
            //            },
            checkcustomer: function (customer) {
                return $http.get(adminurl + "customer/checkcustomer", {
                    params: {
                        'name': customer.name,
                        'contact': customer.contact,
                        'address': customer.address,
                        'company': customer.company,
                        'v': version+''
                    }
                });
            },
            getlastbillnumber: function () {
                return $http.get(adminurl + "invoice/getlastbillnumber", {
                    params: {
        
                        'v': version+''
                    }
                });
            },
            deletecustomerbyid: function (id) {
                return $http.get(adminurl + "customer/deletebyid", {
                    params: {
                        'id': id,
                        'v': version+''
                    }
                });
            },
            editcustomer: function (id, data) {
                return $http.get(adminurl + "customer/updatebyid", {
                    params: {
                        'id': id,
                        'data': data,
                        'v': version+''
                    }
                });
            },
            saveinvoice: function (data) {
                return $http.get(adminurl + "invoice/insert", {
                    params: {
                        'data': data,
                        'v': version+''
                    }
                });
            },
            editinvoice: function (id, data) {
                return $http.get(adminurl + "invoice/updatebyid", {
                    params: {
                        'id': id,
                        'data': data,
                        'v': version+''
                    }
                });
            },
            storeorderproduct: function (data) {
                delete data.sync;
                return $http.get(adminurl + "invoice_product/insert", {
                    params: {
                        'data': data,
                        'v': version+''
                    }
                });
            },
            editorderproduct: function (id, data) {

                delete data.sync;
                delete data.product_name;
                return $http.get(adminurl + "invoice_product/updatebyid", {
                    params: {
                        'id': id,
                        'data': data,
                        'v': version+''
                    }
                });
            },
            backupdbrecord: function (data) {
                return $http.get(adminurl + "backup/insert", {
                    params: {
                        'data': data,
                        'v': version+''
                    }
                });
            },
            getlastbackupdate: function () {
                return $http.get(adminurl + "backup/getlastbackupdate", {
                    params: {
   
                        'v': version+''
                    }
                });
            },
            getinvoice: function (id) {
                return $http.get(adminurl + "invoice/getinvoice", {
                    params: {
                        id: id,
                        'v': version+''
                    }
                });
            },
            updatepayment: function (id, data) {
                return $http.get(adminurl + "invoice/updatebyid", {
                    params: {
                        id: id,
                        data: data,
                        'v': version+''
                    }
                });
            },
            deleteinvoice: function (id) {
                return $http.get(adminurl + "invoice/deleteinvoice", {
                    params: {
                        id: id,
                        'v': version+''
                    }
                });
            },
            getcourierservices: function () {

                return $http({
                    url: adminurl + 'Courierservices?v='+version,
                    method: "GET",
                })

            },
            trackinggetoneby: function (invoiceid) {
                console.log(typeof invoiceid);
                return $http.get(adminurl + "tracking/getoneby", {
                    params: {
                        field: 'invoice_id',
                        value: invoiceid,
                        'v': version+''
                    }
                });

            },
            editbyid: function (editdata) {

                return $http.get(adminurl + "tracking/updatebyid", {
                    params: {
                        id: editdata.id,
                        data: angular.toJson(editdata),
                        'v': version+''
                    }
                });

            },
            insert: function (insertdata) {
                return $http.get(adminurl + "tracking/insert", {
                    params: {
                        data: insertdata,
                        'v': version+''
                    }
                });

            },

            getalldata: function (controller) {
                return $http.get(adminurl + controller, {
                    params: {
 
                        'v': version+''
                    }
                });

            },

            deletebyid: function (controller, id) {
                return $http.get(adminurl + controller + '/deletebyid', {
                    params: {
                        id: id,
                        'v': version+''
                    }
                });

                //                

            },
            insert: function (controller, data) {
                return $http.get(adminurl + controller + '/insert', {
                    params: {
                        data: data,
                        'v': version+''
                    }
                });

                //                

            },
            updatebyid: function (controller, data) {
                return $http.get(adminurl + controller + '/updatebyid', {
                    params: {
                        id: data.id,
                        data: data,
                        'v': version+''
                    }
                });

                //                

            },
            getoneby: function (controller, value, field) {
                return $http.get(adminurl + controller + '/getmanyby', {
                    params: {
                        field: field,
                        value: value,
                        'v': version+''
                    }
                });

            },


        }
    });
